import firebase from "firebase"
import 'firebase/firestore'

const firebaseConfig = {
    apiKey: "AIzaSyCPhxOxZwpdi6v4YRGb4cd4vUJTwM1N1X8",
    authDomain: "simple-contact-form-91db1.firebaseapp.com",
    projectId: "simple-contact-form-91db1",
    storageBucket: "simple-contact-form-91db1.appspot.com",
    messagingSenderId: "947587081270",
    appId: "1:947587081270:web:66acc00773e7fe8be22497"
  };

  //init firebase app
  firebase.initializeApp(firebaseConfig)

  //init firestore
  const projectFirestore = firebase.firestore()

  export { projectFirestore }